package com.example.covidtracking.activity;

import static com.example.covidtracking.Config.myRef;
import static com.example.covidtracking.Config.userID;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;

import com.example.covidtracking.R;
import com.example.covidtracking.adapter.PrescriptionAdapter;
import com.example.covidtracking.adapter.showHistoryAdapter;
import com.example.covidtracking.databinding.ActivityShowHistoryBinding;
import com.example.covidtracking.model.Prescription;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ShowHistoryActivity extends AppCompatActivity {

    ActivityShowHistoryBinding binding;
    showHistoryAdapter adapter;
    List<String > list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(ShowHistoryActivity.this,R.layout.activity_show_history);

        binding.showFeeling.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        myRef.child("User").child(userID).child("feelings").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                try {


                    list.clear();
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        String b = data.getValue(String.class);
                        list.add(b);
                    }
                    adapter = new showHistoryAdapter(ShowHistoryActivity.this, list);
                    binding.showFeeling.setAdapter(adapter);
                }catch (Exception e){

                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                // Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

    }
}