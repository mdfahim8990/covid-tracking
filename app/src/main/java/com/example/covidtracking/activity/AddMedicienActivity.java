package com.example.covidtracking.activity;

import static com.example.covidtracking.Config.myRef;
import static com.example.covidtracking.Config.userID;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.covidtracking.AlarmReceiver;
import com.example.covidtracking.MainActivity;
import com.example.covidtracking.R;
import com.example.covidtracking.adapter.ShowAddMedicienAdapter;
import com.example.covidtracking.adapter.showHistoryAdapter;
import com.example.covidtracking.databinding.ActivityAddMedicienBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddMedicienActivity extends AppCompatActivity {


    ActivityAddMedicienBinding binding;
    ShowAddMedicienAdapter adapter;
    List<String > list = new ArrayList<>();
    private TimePicker alarmTimePicker;
    Calendar calendar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding  = DataBindingUtil.setContentView(this,R.layout.activity_add_medicien);

        String id = getIntent().getStringExtra("id");
        binding.addMedicineBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddMedicienActivity.this);
                View view = LayoutInflater.from(AddMedicienActivity.this).inflate(R.layout.model_add_patient_feel,null);
                builder.setView(view);
                builder.setCancelable(false);


                TimePicker timePicker = view.findViewById(R.id.simpleTimePicker);
                Button btnY = view.findViewById(R.id.model_filling_submit);
                Button btnN = view.findViewById(R.id.model_filling_cancel);
                TextView titleTV = view.findViewById(R.id.model_patient_fill);

                AlertDialog alertDialog = builder.create();
                alertDialog.show();

                btnN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                timePicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                         calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker.getCurrentHour());
                        calendar.set(Calendar.MINUTE, alarmTimePicker.getCurrentMinute());

                    }
                });

                btnY.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                      //  titleTV



                        // Create a new PendingIntent and add it to the AlarmManager
                        Intent intent = new Intent(AddMedicienActivity.this, AlarmReceiver.class);
                        PendingIntent pendingIntent = PendingIntent.getService(AddMedicienActivity.this, 0,intent, 0);

//or if you start an Activity
//PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0,intent, 0);

                        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                        myRef.child("User").child(userID).child("Prescription").child("Medicine").child(id).child(myRef.push().getKey()).setValue(titleTV.getText().toString()+" "+calendar.getTimeInMillis());
                    }
                });
            }
        });

        binding.showMedicine.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        try {
            myRef.child("User").child(userID).child("Prescription").child("Medicine").child(id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.

                    try {


                        list.clear();
                        for (DataSnapshot data : dataSnapshot.getChildren()) {
                            String b = data.getValue(String.class);
                            list.add(b);
                        }
                        adapter = new ShowAddMedicienAdapter(AddMedicienActivity.this, list);
                        binding.showMedicine.setAdapter(adapter);
                    }catch (Exception e){

                    }
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    // Log.w(TAG, "Failed to read value.", error.toException());
                }
            });

        }catch (Exception e ){

        }

    }
}